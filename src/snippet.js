import join from 'url-join' ;
import debug from 'debug';

import { authenticated } from './decorators';

const log = debug('bitbucketjs:snippet');
const path = '/2.0/snippets';

function formify(req, options={}) {
  req.type('multipart/form-data');

  Object.keys(options).forEach(k => {
    if (k !== 'owner' && k !== 'files') {
      req.field(k, options[k]);
    }
  });

  if (options.files) {
    options.files.forEach(f => {
      req.attach('file', f);
    })
  };
}

module.exports = function (opts) {
  const request = opts.request;

  function pathFor(ownername, snippetID) {
    return join(opts.apiRoot, path, ownername, snippetID);
  }

  return {
    fetch(ownername, snippetID) {
       return request
         .get(pathFor(ownername, snippetID), opts.requestData);
    },

    @authenticated(request)
    create(options) {
      const req = request
        .post(join(opts.apiRoot, path, options.owner), opts.requestData)

      formify(req, options);
      return req;
    },

    @authenticated(request)
    update(ownername, snippetID, options) {
      const req = request
        .put(pathFor(ownername, snippetID), opts.requestData);

      formify(req, options);
      return req;
    },

    @authenticated(request)
    delete(ownername, snippetID) {
      return request
        .del(pathFor(ownername, snippetID), opts.requestData);
    }
  }
}
