import urlJoin from 'url-join' ;
import debug from 'debug';
import { authenticated } from './decorators';

const log = debug('bitbucketjs:repo');
const path = '/2.0/teams';

module.exports = function (opts) {
  const request = opts.request;

  return {
    fetch(teamname) {
      return request
        .get(urlJoin(opts.apiRoot, path, teamname), opts.requestData)
    },

    followers(teamname) {
      return request
        .get(urlJoin(opts.apiRoot, path, teamname, 'followers'), opts.requestData)
    },

    @authenticated(request)
    mine(role='member') {
      return request
        .get(urlJoin(opts.apiRoot, path), opts.requestData)
        .query({'role': role})
    }
  }
}
