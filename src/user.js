import urlJoin from 'url-join' ;
import debug from 'debug';

const log = debug('bitbucketjs:repo');
const path = '/2.0/users';

module.exports = function (opts) {
  const request = opts.request;

  return {
    fetch(username) {
       return request
         .get(urlJoin(opts.apiRoot, path, username), opts.requestData)
    },

    followers(username) {
       return request
         .get(urlJoin(opts.apiRoot, path, username, 'followers'), opts.requestData)
    }
  }
}
