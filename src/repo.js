import join from 'url-join' ;
import debug from 'debug';
import { authenticated } from './decorators';

const log = debug('bitbucketjs:repo');
const path = '/2.0/repositories';

function validateSlug(slug) {
  if (slug.split('/').length !== 2) {
    throw new Error('repo string shorthand is owner/repo');
  }
}

module.exports = function (opts) {
  const request = opts.request;

  // api/repositories/username, api/repositories/username/reponame
  function pathFor(slug) {
    return join(opts.apiRoot, path, slug);
  }

  return {
    pathFor,

    fetch(slug) {
       return request
         .get(pathFor(slug), opts.requestData);
    },

    forOwner(owner) {
       return request
         .get(pathFor(owner), opts.requestData);
    },

    @authenticated(request)
    create(slug, opts={}) {
      validateSlug(slug);

      return request
        .post(pathFor(slug), opts.requestData)
        .send(opts);
    },

    @authenticated(request)
    delete(slug, opts={}) {
      validateSlug(slug);

      return request
        .del(pathFor(slug), opts.requestData)
        .send(opts);
    }
  }
}
